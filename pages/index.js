import Head from "next/head";

import Nav from "../components/nav";
import Presentation from "../components/presentation";
import Brands from "../components/brands";

export default function Home() {
  return (
    <div className="container-fluid wrap">
      <Head>
        <title>
          Stiven Martínez | UI/UX / Frontend Dev | GuruHotel Co-founder
        </title>
        <meta
          name="description"
          content="Hi, i'm Stiven Martínez. I make digital products, based in Medellín, Colombia. Brand and UI/UX designer, frontend dev lead and co-founder of GuruHotel. YCombinator Alumni."
        />
        <link rel="icon" href="/images/favicon.png" />
        <script
          async
          src="https://www.googletagmanager.com/gtag/js?id=UA-28485383-1"
        ></script>
        <script
          dangerouslySetInnerHTML={{
            __html: `window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-28485383-1');`,
          }}
        />
      </Head>
      <header className="header row middle-xs">
        <div className="col-xs-3">
          <img className="logo" src="/images/stivmartinez.svg" />
        </div>
        <Nav />
      </header>
      <main className="main">
        <Presentation />
        {/*<Brands />*/}
      </main>
    </div>
  );
}
