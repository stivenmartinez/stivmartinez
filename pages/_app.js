import '../public/sass/style.scss'

export default function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}
