import {
  FaLinkedinIn,
  FaTwitter,
  FaInstagram,
  FaYCombinator,
} from "react-icons/fa";
import { MdTranslate } from "react-icons/md";

export default function Nav() {
  return (
    <nav className="header--nav col-xs-9 end-xs">
      <ul className="social">
        <li>
          <a
            href="https://www.linkedin.com/in/stivenmartinez"
            rel="noopener noreferrer nofollow"
            target="_blank"
            title="LinkedIn"
          >
            <FaLinkedinIn />
          </a>
        </li>
        <li>
          <a
            href="https://twitter.com/stivenmart"
            rel="noopener noreferrer nofollow"
            target="_blank"
            title="Twitter"
          >
            <FaTwitter />
          </a>
        </li>
        <li>
          <a
            href="https://www.instagram.com/stivmartinez"
            rel="noopener noreferrer nofollow"
            target="_blank"
            title="Instagram"
          >
            <FaInstagram />
          </a>
        </li>
        <li>
          <a
            href="https://bookface.ycombinator.com/user/451339"
            rel="noopener noreferrer nofollow"
            target="_blank"
            title="YCombinator"
          >
            <FaYCombinator />
          </a>
        </li>
      </ul>
      <ul className="lang">
        <li>
          <MdTranslate className="icon" />
        </li>
        <li>
          <a href="/">En</a>
        </li>
      </ul>
    </nav>
  );
}
