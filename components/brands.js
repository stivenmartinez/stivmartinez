import { motion } from "framer-motion"

export function Brand({
  logo,
  logoIcon,
  name
}) {
  return (
    <div className="brands__list--brand">
      <div className="brand center-xs">
        <div className={`brand__complete ${name.toLowerCase()}`}>
          <img src={logo} alt={name} />
        </div>
        <div className={`brand__icon ${name.toLowerCase()}`}>
          <img src={logoIcon} alt={name} />
        </div>
      </div>
      <p className="brand__name font-size__small--x">
        {name}
      </p>
    </div>
  )
}

export default function Brands() {

  const variants = {
    visible: {
      opacity: 1,
      translateY: 0,
      transition: {
        when: "beforeChildren",
        staggerChildren: 0.15
      },
    },
    hidden: {
      opacity: 0,
      translateY: 8,
      transition: {
        when: "afterChildren",
      },
    },
  }

  const brandList = [
    { logo: '/images/guruhotel.svg', logoIcon: '/images/guruhotel-icon.svg', name: 'GuruHotel' },
    { logo: '/images/brands/amadezing.svg', logoIcon: '/images/brands/amadezing-icon.svg', name: 'Amadezing' },
    { logo: '/images/brands/betuber.svg', logoIcon: '/images/brands/betuber-icon.svg', name: 'Betuber' },
    { logo: '/images/brands/coodesh.svg', logoIcon: '/images/brands/coodesh-icon.svg', name: 'Coodesh' },
    { logo: '/images/brands/jazzdomain.svg', logoIcon: '/images/brands/jazzdomain-icon.svg', name: 'JazzDomain' },
    { logo: '/images/brands/lacasitadelgato.svg', logoIcon: '/images/brands/lacasitadelgato-icon.svg', name: 'LaCasitaDelGato' },
    { logo: '/images/brands/maxresume.svg', logoIcon: '/images/brands/maxresume-icon.svg', name: 'MaxResume' },
    { logo: '/images/brands/medicinaaldia.svg', logoIcon: '/images/brands/medicinaaldia-icon.svg', name: 'MedicinaAlDia' },
    { logo: '/images/brands/ositomedia.svg', logoIcon: '/images/brands/ositomedia-icon.svg', name: 'OsitoMedia' },
    { logo: '/images/brands/thebratcompany.svg', logoIcon: '/images/brands/thebratcompany-icon.svg', name: 'TheBratCompany' },
    { logo: '/images/brands/themesei.svg', logoIcon: '/images/brands/themesei-icon.svg', name: 'Themesei' },
    { logo: '/images/brands/torniautos.svg', logoIcon: '/images/brands/torniautos-icon.svg', name: 'Torniautos' },
  ]

  return (
    <motion.div
      className="brands row middle-xs center-xs"
      initial="hidden"
      animate="visible"
      variants={variants}
    >
      <div className="col-xs-12 col-md-8 start-xs">
        <h3 className="brands__title">Algunas marcas</h3>
        <div className="row brands__list">
          { brandList &&
            brandList.map((brand, key) => (
              <motion.div
                key={key}
                variants={variants}
                className="col-xs-6 col-sm-4"
              >
                <Brand
                  logo={brand.logo}
                  logoIcon={brand.logoIcon}
                  name={brand.name}
                />
              </motion.div>
            ))
          }
        </div>
      </div>
    </motion.div>
  )
}
