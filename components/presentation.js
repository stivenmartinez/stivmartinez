import { motion } from "framer-motion";

export default function Presentation() {
  const variants = {
    visible: {
      opacity: 1,
      translateY: 0,
      transition: {
        when: "beforeChildren",
        staggerChildren: 0.15,
      },
    },
    hidden: {
      opacity: 0,
      translateY: 8,
      transition: {
        when: "afterChildren",
      },
    },
  };

  return (
    <div className="presentation row middle-xs center-xs">
      <div className="col-xs-11 col-md-8 start-xs">
        <motion.div initial="hidden" animate="visible" variants={variants}>
          <motion.h1
            className="presentation__title font-weight__normal"
            variants={variants}
          >
            Hi, I'm Stiven Martínez ✌️
          </motion.h1>
          <motion.p
            variants={variants}
            className="presentation__text font-size__big"
          >
            I make digital products. I am based in Medellín, Colombia.{" "}
            <strong>Brands and UI / UX designer</strong>, frontend developer
            lead and co-founder of{" "}
            <a
              href="https://guruhotel.com"
              rel="noopener noreferrer nofollow"
              target="_blank"
              title="GuruHotel, Inc"
            >
              GuruHotel, Inc
            </a>
            .
          </motion.p>
          <motion.h2
            variants={variants}
            className="presentation__text font-size__medium text-color__white"
          >
            Alumni{" "}
            <a
              href="https://www.ycombinator.com/companies/13508"
              rel="noopener noreferrer nofollow"
              target="_blank"
              title="GuruHotel, Inc"
            >
              YCombinator
            </a>{" "}
            (W2020)
          </motion.h2>
          <div className="row center-xs margin-top__mega--x">
            <motion.div variants={variants} className="col-xs-12">
              <span className="presentation__experience display__block text-transform__uppercase letter-spacing__big font-size__small--x">
                +10 years of experience / Multiple projects
              </span>
            </motion.div>
          </div>
          <div className="presentation__logos row middle-xs center-xs">
            <motion.div
              variants={variants}
              className="col-xs-6 col-sm-4 center-xs start-sm"
            >
              <a
                href="https://guruhotel.com/"
                rel="noopener noreferrer nofollow"
                target="_blank"
                title="Twitter"
              >
                <img
                  src="/images/guruhotel.svg"
                  className="presentation__logos--logo"
                />
              </a>
            </motion.div>
            <motion.div
              variants={variants}
              className="col-xs-6 col-sm-4 center-xs"
            >
              <a
                href="https://www.ycombinator.com/"
                rel="noopener noreferrer nofollow"
                target="_blank"
                title="Twitter"
              >
                <img
                  src="/images/ycombinator.svg"
                  className="presentation__logos--logo"
                />
              </a>
            </motion.div>
            <motion.div
              variants={variants}
              className="col-xs-6 col-sm-4 center-xs end-sm"
            >
              <a
                href="https://mcontigo.com/"
                rel="noopener noreferrer nofollow"
                target="_blank"
                title="Twitter"
              >
                <img
                  src="/images/mcontigo.svg"
                  className="presentation__logos--logo mcontigo"
                />
              </a>
            </motion.div>
          </div>
        </motion.div>
      </div>
    </div>
  );
}
